package com.example.searchplaces.ui.activity

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.LinearLayout
import com.example.searchplaces.ListAdapter.PlaceDataApapter
import com.example.searchplaces.R
import com.example.searchplaces.controllers.ActivityController
import com.example.searchplaces.models.PlacesData
import com.example.searchplaces.models.VenuesItem
import com.example.searchplaces.presenters.ListPresenter.PlaceDataListPresenter
import com.example.searchplaces.viewModels.ActivityViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    val activityViewModel = ActivityViewModel()
    val activityController = ActivityController()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        initData()
        initViews()
        observeData()

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    private fun initData() {
        recyclerView.setHasFixedSize(true)
        var manager = LinearLayoutManager(this@MainActivity)
        manager.orientation = LinearLayout.VERTICAL
        recyclerView.layoutManager = manager
    }

    private fun initViews() {
        activityController.getListingData(viewModel = activityViewModel)
    }

    private fun observeData() {
        activityViewModel.getBehaviourObserver()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableObserver<PlacesData>() {
                override fun onComplete() {

                }

                override fun onNext(places: PlacesData) {
                    val placeDataListPresenter = PlaceDataListPresenter()
                    placeDataListPresenter.listOfPlaceDataVenue = places.response.venues as ArrayList<VenuesItem>

                    val placeDataApapter = PlaceDataApapter(this@MainActivity, placeDataListPresenter)
                    recyclerView.adapter = placeDataApapter
                    for ((i, v) in places.response.venues!!) {
                        Log.d("BHANU", "VALUE FOR HIT: " + v)
                    }
                }

                override fun onError(e: Throwable) {

                }
            })
    }
}
