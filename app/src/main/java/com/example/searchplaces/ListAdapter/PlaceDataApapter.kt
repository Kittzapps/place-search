package com.example.searchplaces.ListAdapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.searchplaces.R
import com.example.searchplaces.listView.ListViewImpl
import com.example.searchplaces.presenters.ListPresenter.PlaceDataListPresenter
import kotlinx.android.synthetic.main.list_items_layout.view.*

class PlaceDataApapter(
    private var context: Context,
    private var listPresenter: PlaceDataListPresenter
) : RecyclerView.Adapter<PlaceDataApapter.Holder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, pos: Int): Holder {
        var view = LayoutInflater.from(context).inflate(R.layout.list_items_layout, viewGroup, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return listPresenter.getListSize()
    }

    override fun onBindViewHolder(holder: Holder, pos: Int) {
        listPresenter.setRowItems(holder, pos)
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view), ListViewImpl {


        override fun setNameOfStore(name: String) {
            itemView.nameStore.text = name
        }

    }

}