package com.example.searchplaces.interactor.networkClient

import com.example.searchplaces.models.PlacesData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class PlacesDataNetworkResponse :Callback<PlacesData> {
    override fun onFailure(call: Call<PlacesData>, t: Throwable) {

    }

    override fun onResponse(call: Call<PlacesData>, response: Response<PlacesData>) {

    }
}