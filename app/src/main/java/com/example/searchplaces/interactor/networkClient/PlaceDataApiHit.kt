package com.example.searchplaces.interactor.networkClient

import android.util.Log
import com.example.searchplaces.models.PlacesData
import com.google.gson.Gson
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Call
import retrofit2.Response

class PlaceDataApiHit : PlaceDataGateway {
    private val behaviour: BehaviorSubject<PlacesData> = BehaviorSubject.create()

    override fun observePlaceData(): BehaviorSubject<PlacesData> {
        return getData()
    }

    fun getData(): BehaviorSubject<PlacesData> {
        NetworkHit.getClient().GET_PLACES_DATA(
            "40.7484,-73.9857",
            "coffee",
            "10",
            "ICGVMU0BZJGC0MC4HURLUU3GRZ4F5QP04ZX012O1CIGPZFEE",
            "WKRL4RB0T5RS015AK5VE4HLB3ANHGCZVXVE3KQLBZFDXZQ0I",
            "20190528"
        ).enqueue(object : PlacesDataNetworkResponse() {
            override fun onResponse(call: Call<PlacesData>, response: Response<PlacesData>) {
                if (response.body() != null) {
                    val placesData = response.body()
                    if (placesData != null) {
                        Log.d("BHANU","RESPONSE: "+Gson().toJson(placesData))
                        setBehaviourData(placesData)
                    }
                }
            }

            override fun onFailure(call: Call<PlacesData>, t: Throwable) {
                t.stackTrace
            }
        })
        return getBehaviourData()
    }



    private fun getBehaviourData(): BehaviorSubject<PlacesData> {
        return behaviour
    }

    private fun setBehaviourData(placesData: PlacesData) {
        this.behaviour.onNext(placesData)
    }
}