package com.example.searchplaces.interactor.networkClient

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkHit {
    companion object {
        fun getClient(): GetApis {
            return Retrofit.Builder()
                .baseUrl("https://api.foursquare.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(GetApis::class.java)
        }
    }
}