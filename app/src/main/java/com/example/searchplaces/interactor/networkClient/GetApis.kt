package com.example.searchplaces.interactor.networkClient

import com.example.searchplaces.models.PlacesData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GetApis {
//    https://api.foursquare.com/v2/venues/search?
//     ll=40.7484,-73.9857&
//     query=coffe&
//     limit=5&
//     client_id=ICGVMU0BZJGC0MC4HURLUU3GRZ4F5QP04ZX012O1CIGPZFEE&
//     client_secret=WKRL4RB0T5RS015AK5VE4HLB3ANHGCZVXVE3KQLBZFDXZQ0I&
//     v=20190528

    @GET("/v2/venues/search/")
    fun GET_PLACES_DATA(
        @Query("ll") ll: String, @Query("query") query: String, @Query("limit") limit: String = "10", @Query("client_id") client_id: String,
        @Query("client_secret") client_secret: String, @Query("v") v: String ):Call<PlacesData>
}