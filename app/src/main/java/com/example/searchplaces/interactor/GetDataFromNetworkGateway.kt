package com.example.searchplaces.interactor

import com.example.searchplaces.interactor.networkClient.PlaceDataApiHit
import com.example.searchplaces.models.PlacesData
import io.reactivex.subjects.BehaviorSubject

class GetDataFromNetworkGateway {
    var placeDataApiHit = PlaceDataApiHit()

    fun getDataGateway() :BehaviorSubject<PlacesData>{
        return placeDataApiHit.observePlaceData()
    }

}
