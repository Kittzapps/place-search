package com.example.searchplaces.interactor.networkClient

import com.example.searchplaces.models.PlacesData
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

interface PlaceDataGateway {
    fun observePlaceData(): BehaviorSubject<PlacesData>
}
