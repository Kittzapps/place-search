package com.example.searchplaces.controllers

import android.content.Context
import android.support.v7.widget.RecyclerView
import com.example.searchplaces.ListAdapter.PlaceDataApapter
import com.example.searchplaces.interactor.GetDataFromNetworkGateway
import com.example.searchplaces.models.PlacesData
import com.example.searchplaces.presenters.ActivityPresenter
import com.example.searchplaces.presenters.ListPresenter.PlaceDataListPresenter
import com.example.searchplaces.viewModels.ActivityViewModel

class ActivityController {
    private val activityPresenter = ActivityPresenter()
    private val gateway = GetDataFromNetworkGateway()
    private val placeDataListPresenter = PlaceDataListPresenter()
    fun doActionOnClick() {
    }

    fun getListingData(viewModel: ActivityViewModel) {
        activityPresenter.getDataFromGateway(viewModel, gateway.getDataGateway())
    }

    fun setList(context : Context,
        recyclerView: RecyclerView,
        places: PlacesData
    ) {
        recyclerView.adapter = PlaceDataApapter(context,placeDataListPresenter )

    }
}