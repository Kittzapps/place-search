package com.example.searchplaces.models


import com.google.gson.annotations.SerializedName

data class VenuesItem(@SerializedName("id")
                      val id: String = "",
                      @SerializedName("name")
                      val name: String = "",
                      @SerializedName("location")
                      val location: Location,
                      @SerializedName("categories")
                      val categories: List<CategoriesItem>?,
                      @SerializedName("referralId")
                      val referralId: String = "",
                      @SerializedName("hasPerk")
                      val hasPerk: Boolean = false)


data class Response(@SerializedName("venues")
                    val venues: List<VenuesItem>?)


data class Meta(@SerializedName("code")
                val code: Int = 0,
                @SerializedName("requestId")
                val requestId: String = "")


data class CategoriesItem(@SerializedName("id")
                          val id: String = "",
                          @SerializedName("name")
                          val name: String = "",
                          @SerializedName("pluralName")
                          val pluralName: String = "",
                          @SerializedName("shortName")
                          val shortName: String = "",
                          @SerializedName("icon")
                          val icon: Icon,
                          @SerializedName("primary")
                          val primary: Boolean = false)


data class LabeledLatLngsItem(@SerializedName("label")
                              val label: String = "",
                              @SerializedName("lat")
                              val lat: Double = 0.0,
                              @SerializedName("lng")
                              val lng: Double = 0.0)


data class Icon(@SerializedName("prefix")
                val prefix: String = "",
                @SerializedName("suffix")
                val suffix: String = "")


data class PlacesData(@SerializedName("meta")
                      val meta: Meta,
                      @SerializedName("response")
                      val response: Response)


data class Location(@SerializedName("address")
                    val address: String = "",
                    @SerializedName("crossStreet")
                    val crossStreet: String = "",
                    @SerializedName("lat")
                    val lat: Double = 0.0,
                    @SerializedName("lng")
                    val lng: Double = 0.0,
                    @SerializedName("labeledLatLngs")
                    val labeledLatLngs: List<LabeledLatLngsItem>?,
                    @SerializedName("distance")
                    val distance: Int = 0,
                    @SerializedName("postalCode")
                    val postalCode: String = "",
                    @SerializedName("cc")
                    val cc: String = "",
                    @SerializedName("neighborhood")
                    val neighborhood: String = "",
                    @SerializedName("city")
                    val city: String = "",
                    @SerializedName("state")
                    val state: String = "",
                    @SerializedName("country")
                    val country: String = "",
                    @SerializedName("formattedAddress")
                    val formattedAddress: List<String>?)


