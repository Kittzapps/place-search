package com.example.searchplaces.presenters.ListPresenter

import com.example.searchplaces.ListAdapter.PlaceDataApapter
import com.example.searchplaces.models.VenuesItem

class PlaceDataListPresenter {
    var listOfPlaceDataVenue = ArrayList<VenuesItem>()

    fun getListSize(): Int {
        return listOfPlaceDataVenue.size
    }

    fun setRowItems(viewHolder: PlaceDataApapter.Holder, pos: Int) {
        viewHolder.setNameOfStore(listOfPlaceDataVenue[pos].name)
    }
}