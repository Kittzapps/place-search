package com.example.searchplaces.presenters

import com.example.searchplaces.models.PlacesData
import com.example.searchplaces.viewModels.ActivityViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

class ActivityPresenter {
    fun getDataFromGateway(viewModel: ActivityViewModel,behaviorSubject: BehaviorSubject<PlacesData>) {
        behaviorSubject.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object :
            DisposableObserver<PlacesData>() {
            override fun onComplete() {

            }

            override fun onNext(t: PlacesData) {
                viewModel.setDataForEmittes(t)
            }

            override fun onError(e: Throwable) {
            }

        })
    }

}