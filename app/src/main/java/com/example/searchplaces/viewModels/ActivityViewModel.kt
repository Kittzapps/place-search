package com.example.searchplaces.viewModels

import com.example.searchplaces.models.PlacesData
import io.reactivex.subjects.BehaviorSubject

class ActivityViewModel {
    private val behaviorSubject: BehaviorSubject<PlacesData> = BehaviorSubject.create()

    fun getBehaviourObserver(): BehaviorSubject<PlacesData> {
        return behaviorSubject
    }

    fun setDataForEmittes(placesData: PlacesData) {
        behaviorSubject.onNext(placesData)
    }

}